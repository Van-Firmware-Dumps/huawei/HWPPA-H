#!/usr/bin/env python
"""
Copyright © Huawei Technologies Co., Ltd. 2010-2019. All rights reserved.
description     :   analysis guas dump bin
modify  record  :   2016-01-22 create file
"""

import struct
import os
import sys
import string
from nas_pid import *
from at_css_msg import *
from at_imsa_msg import *
from at_mn_msg import *
from at_mta_msg import *
from at_ndis_msg import *
from at_oam_msg import *
from at_phy_msg import *
from at_rabm_msg import *
from at_rnic_msg import *
from at_xpds_msg import *
from at_at_msg import *
from at_ccm_msg import *
from at_drvagent_msg import *
from at_ftm_msg import *
from at_l4a_msg import *
from at_mma_msg import *
from at_ppp_msg import *
from at_taf_dsm_msg import *

MACRO_AT_MAX_LOG_MSG_NUM   = 100
MACRO_AT_MSG_INFO_SIZE     = 16

MACRO_AT_MAX_PORT_NUM      = 37
MACRO_AT_PORT_INFO_SIZE    = 16
MACRO_AT_CMD_NAME_LEN      = 12

MACRO_AT_VCOM_EXT_OFF_SIZE = 2
MACRO_AT_VCOM_EXT_ON_SIZE  = 53
MACRO_AT_DMS_STATUS_SIZE   = 8
MACRO_AT_VCOM_STATUS_NUM   = 2
MACRO_AT_COM_STATUS_NUM    = 97

def get_at_msg_str( pid, ulMsgId):
        if ( 'i0_xpds' == pid.lower() or 'i1_xpds' == pid.lower() or 'i2_xpds' == pid.lower()):
                return get_at_xpds_msg_str(ulMsgId)
        elif ( 'rnic' == pid.lower()):
                return get_at_rnic_msg_str(ulMsgId)
        elif ( 'rabm' == pid.lower() or 'i1_rabm' == pid.lower()):
                return get_at_rabm_msg_str(ulMsgId)
        elif ( 'i1_stk' == pid.lower() or 'i1_pih' == pid.lower() or 'i1_pb' == pid.lower()):
                return get_at_oam_msg_str(ulMsgId)
        elif ( 'si_stk' == pid.lower() or 'si_pih' == pid.lower() or 'si_pb' == pid.lower()):
                return get_at_oam_msg_str(ulMsgId)
        elif ( 'i2_stk' == pid.lower() or 'i2_pih' == pid.lower() or 'i2_pb' == pid.lower()):
                return get_at_oam_msg_str(ulMsgId)
        elif ( 'ndis' == pid.lower()):
                return get_at_ndis_msg_str(ulMsgId)
        elif ( 'i0_imsa' == pid.lower() or 'i1_imsa' == pid.lower()):
                return get_at_imsa_msg_str(ulMsgId)
        elif ( 'css' == pid.lower()):
                return get_at_css_msg_str(ulMsgId)
        elif ( 'mta' == pid.lower() or 'i1_mta' == pid.lower() or 'i2_mta' == pid.lower()):
                return get_at_mta_msg_str(ulMsgId)
        elif ( 'mma' == pid.lower() or 'i1_mma' == pid.lower() or 'i2_mma' == pid.lower()):
                return get_at_mma_msg_str(ulMsgId)
        elif ( 'at' == pid.lower()):
                return get_at_at_msg_str(ulMsgId)
        elif ( 'i0_ccm' == pid.lower() or 'i1_ccm' == pid.lower() or 'i2_ccm' == pid.lower()):
                return get_at_ccm_msg_str(ulMsgId)
        elif ( 'i0_drv_agent' == pid.lower() or 'i1_drv_agent' == pid.lower() or 'i2_drv_agent' == pid.lower()):
                return get_at_drvagent_msg_str(ulMsgId)
        elif ( 'i0_sys_ftm' == pid.lower() or 'i1_sys_ftm' == pid.lower()):
                return get_at_ftm_msg_str(ulMsgId)
        elif ( 'i0_l4_l4a' == pid.lower() or 'i1_l4_l4a' == pid.lower()):
                return get_at_l4a_msg_str(ulMsgId)
        elif ( 'ppp' == pid.lower()):
                return get_at_ppp_msg_str(ulMsgId)
        elif ( 'taf' == pid.lower() or 'i1_taf' == pid.lower() or 'i2_taf' == pid.lower()):
                return get_at_taf_dsm_msg_str(ulMsgId)
        elif ( 'i0_dsm' == pid.lower() or 'i1_dsm' == pid.lower() or 'i2_dsm' == pid.lower()):
                return get_at_taf_dsm_msg_str(ulMsgId)
        else:
                return 'none'

app_status_id_enum_table = {
     0:	"DMS_APP_REG_RD_CB_ERR",
     1:	"DMS_APP_WRT_CB_ERR",
}

com_status_id_enum_table = {
     0: "DMS_ACM_REG_RD_CB_ERR",
     1: "DMS_ACM_RD_CB",
     2: "DMS_ACM_BUFF_NULL",
     3: "DMS_ACM_RD_BUFF_ERR",
     4: "DMS_ACM_RETURN_BUFF_ERR",
     5: "DMS_ACM_OPEN_ERR",
     6: "DMS_ACM_CLOSE_ERR",
     7: "DMS_ACM_REG_RELLOC_BUFF_ERR",
     8: "DMS_ACM_REG_WRT_COPY_ERR",
     9: "DMS_ACM_EVT_CB",
     10: "DMS_ACM_REG_EVT_CB_ERR",
     11: "DMS_ACM_WRT_ASYNC",
     12: "DMS_ACM_WRT_BUFF_ERR",
     13: "DMS_ACM_WRT_CHAN_STAT_ERR",
     14: "DMS_ACM_WRT_ASYNC_ERR",
     15: "DMS_ACM_WR_CB",
     16: "DMS_ACM_WR_DONE_SIZE_ERR",
     17: "DMS_ACM_REG_WRT_CB_ERR",
     18: "DMS_NCM_CTRL_OPEN_ERR",
     19: "DMS_NCM_CTRL_CONN_STUS",
     20: "DMS_NCM_CTRL_CONN_STUS_ERR",
     21: "DMS_NCM_CTRL_REG_CONN_STUS_ERR",
     22: "DMS_NCM_CTRL_RD_CB",
     23: "DMS_NCM_CTRL_REG_RD_CB_ERR",
     24: "DMS_NCM_CTRL_RD_DATA_NULL",
     25: "DMS_NCM_CTRL_RD_LEN_INVALID",
     26: "DMS_NCM_CTRL_WRT_CB",
     27: "DMS_NCM_CTRL_REG_WRT_CB_ERR",
     28: "DMS_NCM_CTRL_WRT_ASYNC",
     29: "DMS_NCM_CTRL_WRT_GET_BUF_ERR",
     30: "DMS_NCM_CTRL_WRT_CHAN_STAT_ERR",
     31: "DMS_NCM_CTRL_WRT_ASYNC_ERR",
     32: "DMS_NCM_CTRL_CLOSE_ERR",
     33: "DMS_NCM_DATA_OPEN_ERR",
     34: "DMS_NCM_DATA_SET_IPV6_ERR",
     35: "DMS_NCM_DATA_FLOW_CTRL_ERR",
     36: "DMS_NCM_DATA_CONN_SPEED_ERR",
     37: "DMS_NCM_DATA_CONN_NTF_ERR",
     38: "DMS_NCM_DATA_CLOSE_ERR",
     39: "DMS_MDM_OPEN_ERR",
     40: "DMS_MDM_RD_CB",
     41: "DMS_MDM_REG_RD_CB_ERR",
     42: "DMS_MDM_RD_ADDR_INVALID",
     43: "DMS_MDM_REG_RD_BUFF_ERR",
     44: "DMS_MDM_FREE_CB",
     45: "DMS_MDM_REG_FREE_CB_ERR",
     46: "DMS_MDM_REG_WRT_COPY_ERR",
     47: "DMS_MDM_RD_MSC",
     48: "DMS_MDM_RD_MSC_NULL",
     49: "DMS_MDM_REG_RD_MSC_ERR",
     50: "DMS_MDM_WRT_MSC",
     51: "DMS_MDM_WRT_MSC_ERR",
     52: "DMS_MDM_REG_RELLOC_BUFF_ERR",
     53: "DMS_MDM_WRT_ASYNC",
     54: "DMS_MDM_WRT_BUFF_ERR",
     55: "DMS_MDM_WRT_ASYNC_ERR",
     56: "DMS_MDM_CLOSE_ERR",
     57: "DMS_UART_REG_RD_CB_ERR",
     58: "DMS_UART_RD_CB",
     59: "DMS_UART_RD_ADDR_INVALID",
     60: "DMS_UART_REG_RD_BUFF_ERR",
     61: "DMS_UART_OPEN_ERR",
     62: "DMS_UART_WRT_SYNC",
     63: "DMS_UART_WRT_SYNC_ERR",
     64: "DMS_HSUART_REG_RD_CB_ERR",
     65: "DMS_HSUART_RD_CB",
     66: "DMS_HSUART_RD_ADDR_INVALID",
     67: "DMS_HSUART_REG_RD_BUFF_ERR",
     68: "DMS_HSUART_WRT_ASYNC",
     69: "DMS_HSUART_WRT_ASYNC_ERR",
     70: "DMS_HSUART_ALLOC_BUF_ERR",
     71: "DMS_HSUART_OPEN_ERR",
     72: "DMS_HSUART_REG_RELLOC_BUFF_ERR",
     73: "DMS_HSUART_FREE_CB",
     74: "DMS_HSUART_REG_FREE_CB_ERR",
     75: "DMS_HSUART_SWITCH_MODE",
     76: "DMS_HSUART_REG_SWITCH_MODE_ERR",
     77: "DMS_HSUART_WATER_CB",
     78: "DMS_HSUART_WATER_LEVEL_ERR",
     79: "DMS_HSUART_REG_WATER_CB_ERR",
     80: "DMS_HSUART_RD_MSC",
     81: "DMS_HSUART_RD_MSC_NULL",
     82: "DMS_HSUART_REG_RD_MSC_ERR",
     83: "DMS_HSUART_WRT_MSC_ERR",
     84: "DMS_HSUART_FLOW_CONTRL_ERR",
     85: "DMS_HSUART_SET_WLEN_ERR",
     86: "DMS_HSUART_SET_STP_ERR",
     87: "DMS_HSUART_SET_EPS_ERR",
     88: "DMS_HSUART_SET_BAUD_ERR",
     89: "DMS_HSUART_SET_ACSHELL_ERR",
     90: "DMS_HSUART_FLUSH_BUFF_ERR",
     91: "DMS_SOCK_RD_CB",
     92: "DMS_SOCK_RD_DATA_NULL",
     93: "DMS_SOCK_RD_LEN_INVALID",
     94: "DMS_SOCK_BSP_SUPPORT_ERR",
     95: "DMS_SOCK_WRT_ASYNC",
     96: "DMS_SOCK_WRT_ASYNC_ERR",
}

def get_mntn_status_id_str(ulStatusId, ulVcomFlag):
    status_id_enum_table = com_status_id_enum_table

    if (ulVcomFlag == 1):
        status_id_enum_table = app_status_id_enum_table

    for statusIndex in status_id_enum_table.keys():
        if statusIndex == ulStatusId:
            return status_id_enum_table[statusIndex]


def analysis_at_mntn_per_rec_msg_info(index, instream, fileLocalOffset, outstream):
        instream.seek(fileLocalOffset)

        (ulSendPid,)        = struct.unpack('I', instream.read(4))
        (ulMsgId,)          = struct.unpack('I', instream.read(4))
        (ulSliceStart,)     = struct.unpack('I', instream.read(4))
        (ulSliceEnd,)       = struct.unpack('I', instream.read(4))

        strSendPid      = guas_get_pid_str(ulSendPid)
        strMsgId        = get_at_msg_str(strSendPid, ulMsgId)

        strSendPid      = '%s(0x%x)' % ( strSendPid, ulSendPid)
        strMsgId        = '%s(0x%x)' % ( strMsgId, ulMsgId)
        strStartSlice   = '0x%x'% ulSliceStart
        strEndSlice     = '0x%x'% ulSliceEnd

        outstream.writelines(["%-6s%-20s%-60s%-20s%-20s\n" % (index, strSendPid, strMsgId, strStartSlice, strEndSlice)])

def analysis_at_mntn_per_rec_port_info(index, instream, fileLocalOffset, outstream):
        instream.seek(fileLocalOffset)

        (strCmdName,)      = struct.unpack('12s', instream.read(12))
        (ulClientStatus,)  = struct.unpack('I', instream.read(4))
        outstream.writelines(["%-6d%-60s%-20d\n" % (index, strCmdName, ulClientStatus)])

def analysis_at_mntn_port_status_info(ulStatusId, instream, fileLocalOffset, ulVcomFlag, outstream):
        instream.seek(fileLocalOffset)

        strStatusName   = get_mntn_status_id_str(ulStatusId, ulVcomFlag)
        (ulStatsCount,) = struct.unpack('I', instream.read(4))
        (ulTime,)       = struct.unpack('I', instream.read(4))

        outstream.writelines(["%-20d%-40s%-20d%-20d\n" % (ulStatusId, strStatusName, ulStatsCount, ulTime)])

def analysis_at_mntn_dms_status_info(ulVcomPortNum, instream, fileLocalOffset, outstream):
        instream.seek(fileLocalOffset)

        outstream.writelines(["\n************ APP STATUS INFO begin!************\n"])
        outstream.writelines(["%-20s%-20s%-40s%-20s%-20s\n" % ("appIdx", "statusId","statusName", "statsCount", "time")])
        for ulVcomPortIdx in range(ulVcomPortNum):
            for ulVcomStatusIdx in range(MACRO_AT_VCOM_STATUS_NUM):
                outstream.writelines(["%-20d" % (ulVcomPortIdx)])
                analysis_at_mntn_port_status_info(ulVcomStatusIdx, instream, fileLocalOffset, 1, outstream)
                fileLocalOffset = fileLocalOffset + MACRO_AT_DMS_STATUS_SIZE

        outstream.writelines(["\n************ APP STATUS INFO end!************\n"])

        outstream.writelines(["\n************ COM STATUS INFO begin!************\n"])
        outstream.writelines(["%-20s%-40s%-20s%-20s\n" % ("statusId","statusName", "statsCount", "time")])
        for ulComStatusIdx in range(MACRO_AT_COM_STATUS_NUM):
            analysis_at_mntn_port_status_info(ulComStatusIdx, instream, fileLocalOffset, 0, outstream)
            fileLocalOffset = fileLocalOffset + MACRO_AT_DMS_STATUS_SIZE

        outstream.writelines(["\n************ COM STATUS INFO end!************\n"])

def analysis_at_msg_port_dump_info( instream, fileOffset, outstream):
        ulMsgLooper        = 0
        ulPortLooper       = 0
        ulVcomPortNum      = MACRO_AT_VCOM_EXT_OFF_SIZE

        outstream.writelines(["%-6s%-20s%-60s%-20s%-20s\n" % ("index", "ulSendPid", "ulMsgName", "ulSliceStart", "ulSliceEnd")])
        while ulMsgLooper < MACRO_AT_MAX_LOG_MSG_NUM:
            ulLooperIndex = ulMsgLooper % MACRO_AT_MAX_LOG_MSG_NUM
            fileLocalOffset = fileOffset + ulLooperIndex * MACRO_AT_MSG_INFO_SIZE
            analysis_at_mntn_per_rec_msg_info(ulMsgLooper, instream, fileLocalOffset, outstream)
            ulMsgLooper = ulMsgLooper + 1

        fileLocalOffset = fileOffset + MACRO_AT_MAX_LOG_MSG_NUM * MACRO_AT_MSG_INFO_SIZE

        instream.seek(fileLocalOffset)
        (ulCurIndex,) = struct.unpack('I', instream.read(4))
        outstream.writelines(["\n last proc msg index is %d.\n\n" % (ulCurIndex - 1)])

        #offset add index len and reserv len
        fileLocalOffset = fileLocalOffset + 8
        instream.seek(fileLocalOffset)
        (ulVersion,)  = struct.unpack('I', instream.read(4))
        (ulVcomExtFlag,)  = struct.unpack('I', instream.read(4))
        outstream.writelines(["%-20s%-20s\n" % ("version", "vcomExtFlag")])
        outstream.writelines(["%-20d%-20d\n" % (ulVersion, ulVcomExtFlag)])

        if (ulVcomExtFlag == 1):
            ulVcomPortNum = MACRO_AT_VCOM_EXT_ON_SIZE

        fileLocalOffset = fileLocalOffset + 8
        analysis_at_mntn_dms_status_info(ulVcomPortNum, instream, fileLocalOffset, outstream)

        fileLocalOffset = fileLocalOffset + (ulVcomPortNum * MACRO_AT_VCOM_STATUS_NUM + MACRO_AT_COM_STATUS_NUM) * MACRO_AT_DMS_STATUS_SIZE

        outstream.writelines(["%-6s%-60s%-20s\n" % ("index", "aucCmdName", "ulClientStatus")])
        while ulPortLooper < MACRO_AT_MAX_PORT_NUM:
            analysis_at_mntn_per_rec_port_info(ulPortLooper, instream, fileLocalOffset, outstream)
            fileLocalOffset = fileLocalOffset + MACRO_AT_PORT_INFO_SIZE
            ulPortLooper = ulPortLooper + 1


def analysis_at_dump_info( infile, offset, outfile):
        instream = infile
        outstream  = outfile
        fileOffset = eval(offset)

        ulbeginTick     = 0
        ulEndTick       = 0

        outstream.writelines(["\n**************************** analysis_at_dump_info begin!*******************************\n"])
        global GLOBAL_Offset

        instream.seek(fileOffset)
        (ulBeginTick,)       = struct.unpack('I', instream.read(4))
        strBeginTick         = '%x'% ulBeginTick
        print ("AT DUMP Begin tag is %s" % (strBeginTick))

        #在0xaa55aa55之后有4字节的reserve位，所以偏移需要加8
        fileOffset = fileOffset + 8

#       Old Version begin is 0xaa55aa55
        global Global_Version
        if ulBeginTick == 2857740885:
            Global_Version = 0x01
            analysis_at_msg_port_dump_info(instream, fileOffset, outstream)

        #2857740885 = 0xaa55aa55 find end tick
        while ulEndTick != 2857740885:
                (ulEndTick,)       = struct.unpack('I', instream.read(4))

        strEndTick         = '%x'% ulEndTick
        print ("AT DUMP End tag is %s" % (strEndTick))


        outstream.writelines(["\n**************************** analysis_at_dump_info end!*******************************\n"])

        return True

########################################################################################
def entry_0x1200003(infile, field, offset, len, version, mode, outfile):
        ########check parameter start#############
        if not field == '0x1200003':
            print ("hidis field is %s" % (field))
            print ("current field is 0x1200003")
            return error['ERR_CHECK_FIELD']
        elif not version == '0x0000':
            print ("hidis version is %s" % (version))
            print ("current version is 0x0000")
            return error['ERR_CHECK_VERSION']
        #########check parameter end##############
        ret = analysis_at_dump_info( infile, offset, outfile)

        #c = msvcrt.getch()
        return 0

